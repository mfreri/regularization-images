# Regularization Images

This repository contains images in resolution 512x512 pixels.

Most of the images were generated using Stable Diffusion 1.5, with inference steps of 50 and guidance scale of 10.

Other images were downloaded from images banks like [Pexels](https://pexels.com/) and [PxHere](https://pxhere.com/).